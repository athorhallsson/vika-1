#include "personrepository.h"

bool alphabeticalOrder(const Person &p1, const Person &p2) {return p1.getName() < p2.getName();}
bool reverseAlphabeticalOrder(const Person &p1, const Person &p2) {return p1.getName() > p2.getName();}
bool birthOrder(const Person &p1, const Person &p2) {return p1.getBirthyear() < p2.getBirthyear();}
bool reverseBirthOrder(const Person &p1, const Person &p2) {return p1.getBirthyear() > p2.getBirthyear();}
bool genderOrder(const Person &p1, const Person &p2) {return p1.getGender() < p2.getGender();}
bool reverseGenderOrder(const Person &p1, const Person &p2) {return p1.getGender() > p2.getGender();}

PersonRepository::PersonRepository() {
     personVector = vector<Person>();
}

void PersonRepository::add(Person& p) {
    personVector.push_back(p);
}

Person PersonRepository::getPersonAtIndex(int index) {
    return personVector[index];
}

void PersonRepository::clear() {
    personVector.clear();
}

unsigned int PersonRepository::getSize() {
    return personVector.size();
}

void PersonRepository::remove(int index) {
    personVector.erase(personVector.begin()+(index));
}

void PersonRepository::sortAlphabetically() {
    sort(personVector.begin(), personVector.end(), alphabeticalOrder);
}

void PersonRepository::sortReverseAlphabetically() {
    sort(personVector.begin(), personVector.end(), reverseAlphabeticalOrder);
}

void PersonRepository::sortByBirthyear() {
    sort(personVector.begin(), personVector.end(), birthOrder);
}

void PersonRepository::sortByReverseBirthyear() {
    sort(personVector.begin(), personVector.end(), reverseBirthOrder);
}

void PersonRepository::sortByGender() {
    sort(personVector.begin(), personVector.end(), genderOrder);
}

void PersonRepository::sortByReverseGender() {
    sort(personVector.begin(), personVector.end(), reverseGenderOrder);
}
