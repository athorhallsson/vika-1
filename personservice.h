#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "personrepository.h"

class PersonService {
public:
    PersonService();
    void add(Person p);
    Person getPersonAtIndex(int index);
    bool save();
    bool load();
    void clear();
    vector<int> find(const string &searchName);
    void remove(int index);
    void sortAlphabetically();
    void sortReverseAlphabetically();
    void sortByBirthyear();
    void sortByReverseBirthyear();
    void sortByGender();
    void sortByReverseGender();
    unsigned int getSize();
private:
    PersonRepository personRepo;

};

#endif // PERSONSERVICE_H
