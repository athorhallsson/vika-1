
#include "personservice.h"

PersonService::PersonService() {
    personRepo = PersonRepository();
}

void PersonService::add(Person p) {
    personRepo.add(p);
    save();
}

bool PersonService::save() {
    ofstream list ("list.txt");
    if (list.is_open()) {
        for (unsigned int i = 0; i < personRepo.getSize(); i++) {
            list << personRepo.getPersonAtIndex(i);
        }
        list.close();
        return true;
    }
    return false;
}

bool PersonService::load() {
    string name, birthYear, deathYear, gender;
    ifstream list ("list.txt");
    if (list.is_open()) {
        while (getline(list, name, '\t')) {
            getline(list, birthYear, '\t');
            getline(list, deathYear, '\t');
            getline(list, gender, '\t');
            Person p(name, birthYear, deathYear, gender);
            personRepo.add(p);
        }
        list.close();
        return true;
    }
    return false;
}

vector<int> PersonService::find(const string& searchName) {
    vector<int> locationVector(0);
    for (unsigned int i = 0; i < personRepo.getSize(); i++) {
        unsigned int pos;
        pos = personRepo.getPersonAtIndex(i).getName().find(searchName);
        if (pos < personRepo.getPersonAtIndex(i).getName().size()) {       //find() returns pos > size() if not found
            locationVector.push_back(i);
        }
    }
    return locationVector;
}

void PersonService::clear() {
    personRepo.clear();
}

void PersonService::remove(int index) {
   personRepo.remove(index);
}

Person PersonService::getPersonAtIndex(int index) {
    return personRepo.getPersonAtIndex(index);
}

void PersonService::sortAlphabetically() {
    personRepo.sortAlphabetically();
}

void PersonService::sortReverseAlphabetically() {
    personRepo.sortReverseAlphabetically();
}

void PersonService::sortByBirthyear() {
    personRepo.sortByBirthyear();
}

void PersonService::sortByReverseBirthyear() {
    personRepo.sortByReverseBirthyear();
}

void PersonService::sortByGender() {
    personRepo.sortByGender();
}

void PersonService::sortByReverseGender() {
    personRepo.sortByReverseGender();
}

unsigned int PersonService::getSize() {
    return personRepo.getSize();
}
