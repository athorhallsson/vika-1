#ifndef PERSON_H
#define PERSON_H

#include <iostream>
using namespace std;

class Person
{
public:
    Person();
    Person(const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender);
    string getName() const;
    string getBirthyear() const;
    string getDeathyear() const;
    string getGender() const;
    friend ostream& operator << (ostream& out, const Person& p);
    friend istream& operator >> (istream& in, Person& p);
private:
    string name;
    string birthYear;
    string deathYear;
    string gender;
};

#endif // PERSON_H
